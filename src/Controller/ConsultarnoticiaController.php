<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;


use App\Services\Consultar;
use Symfony\Component\HttpFoundation\JsonResponse;

class ConsultarnoticiaController extends AbstractController
{
    /**
     * @Route("/consultarnoticia", name="consultarnoticia")
     */
    public function index()
    {
        return $this->render('consultarnoticia/index.html.twig', [
            'controller_name' => 'ConsultarnoticiaController',
        ]);
    }

    public function Consultanoticia(Consultar $consultar){
        
        
        $noticia = $consultar->getNoticias();
        
       foreach ($noticia as $noticia[0]['articles'] => $articles) {
           foreach ($articles as $articles['title'] => $titulo) {
                foreach ($titulo as $noticias ) {
                    return $noticia;
                }
           }
       }
       
        
        return $noticias;
        return $this->render('consultarnoticia/index.html.twig', [
            'noticia1' => $noticias,
        ]);
         
        
    }
}
