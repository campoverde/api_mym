<?php
namespace App\Services;


use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Consultar{


public $api;
public $response;


    public function getNoticias(){
            //API URL
      $url = 'https://newsapi.org/v2/top-headlines?country=ar&category=entertainment&apiKey=981bf0304e5d42b39e97c7f437e70116';

      //create a new cURL resource
      $ch = curl_init($url);

      
      //set the content type to application/json
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

      //return response instead of outputting
      
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HEADER, 0);

      //execute the POST request
      $result = curl_exec($ch);

      //close cURL resource
      curl_close($ch);
      $data = json_decode($result,true);

      return new JsonResponse(
        [
            'message' => 'There are no jobs in the database', $data
        ]
        
   ); 
    }

  public function Noticias(){
    $notice=$this->getNoticias();
    
   
    $data = array(
        'noticia' =>$notice['0']['artiles']['title'],
        'descripcion'=>$notice['0']['artiles']['description'],
        'imagen'=>$notice['0']['artiles']['urlToImage'],
        'fecha'=>$notice['0']['artiles']['publishedAt'],
        'articulo'=>$notice['0']['artiles']['content']
    );

 
    return $data;
  }
}